# Ğ1-Cotis

Ğ1-Cotis est un script de **cotisation volontaire**, qui permet de verser un % des paiements reçus aux comptes que l'on souhaite.
Il dépend de Silkaj et Diceware. Il permet aussi de programmer des **versements récurrents**.

Lancé en tâche cron, il est prévu pour agir périodiquement (jours, semaine, mois, reboot) :

## Cotisation
* vérification du montant d'un compte intermédiaire ;
* envoi d'une cotisation à différents comptes "de cotisation" ;
* envoi du reste de la transaction sur un compte destinataire.

## Virement récurrent
* envoi d'un montant fixe depuis un compte "de réserve" vers un compte destinataire.

Les comptes de réception des cotisations peuvent être paramétrés, ainsi que les pourcentages de cotisation.
On peut gérer plusieurs comptes G1cotis, sur plusieurs fréquences, et donc proposer ce service à son entourage / groupe local.
On crée un nouveau compte avec le script nouveau-compte.sh

Vous trouverez (un peu) plus d'informations sur [le site](g1pourboire.fr/G1cotis.html).

## Installation

Il vous faut tout d'abord installer les dépendances :

    Diceware
    bc

* Sous Debian 11  ou Ubuntu 21.04 GNU/Linux:  
`# apt update ; apt upgrade ; apt install diceware bc silkaj`  

* Sous d'autres distributions : je ne sais pas, indiquez-moi comment vous faites ! Voici [le site de Silkaj](https://silkaj.duniter.org/index.html)

* vous pouvez également utiliser pip:
`# pip3 install silkaj==0.9.0`

* vérifiez que vous avez bien **silkaj en version 0.8, 0.9 ou 0.10** (0.11 et + ne sont pas gérées):
`$ silkaj --version`

Ensuite :

* Placez-vous dans le /home de l'utilisateur qui va lancer Ğ1cotis, connecté comme cet utilisateur.  
* Télécharger la dernière release (0.3.1) en .tar.gz :  
`$ wget https://git.duniter.org/matograine/g1-cotis/-/archive/master/g1-cotis-master.zip`
* La déziper où vous le souhaitez :  
`$ tar -xf g1-cotis-master.tar.gz`
* Renommer (si vous le souhaitez) :  
`$ mv g1-cotis-master G1cotis`
* Rendez ce dossier inaccessible pour les autres utilisateurs :  
`$ chmod 700 G1cotis`
* Vous placer dans le dossier :  
`$ cd G1cotis`
* Editer le fichier config.sh en fonction des paramètres locaux (dossier, noeud, monnaie, dossier **silkaj-g1cotis-multioutput**) :  
`$ cp config.sh.example config.sh ; nano config.sh`
* Créer le premier compte (vous devez être dans le dossier G1cotis):  
`$ bash nouveau-compte.sh`
* Modifiez le crontab en suivant les recommandations du script :  
`$ crontab -e `

Tous les dossiers nécessaires seront créés à la première création de compte. 

Pour une mise à jour, déplacez simplement les dossiers d_\<periode\> ; auth_g1cotis et le config.sh de l'ancien dossier vers le nouveau.

## Mise à jour depuis la version 3.1
```
# faites une sauvegarde
cp G1cotis G1cotis.OLD

# mettez à jour Silkaj
sudo apt upgrade silkaj
# Ou installez-le
sudo apt install silkaj
silkaj --version # doit indiquer la version 0.8 ou 0.9 

# Installez la version courante
wget https://git.duniter.org/matograine/g1-cotis/-/archive/master/g1-cotis-master.tar.gz
tar -xf g1-cotis-master.tar.gz

# déplacez les fichiers modifiés
cp  -r g1-cotis-master/* G1cotis/
```

## Configuration

Des questions vous seront posées à l'éxécution de `bash nouveau-compte.sh`, il vous suffit d'y répondre.

Attention : Ğ1Cotis ne gère pas des noms de compte comprenant une espace. `Julie Martin` ne fonctionnera pas, mais `JulieMartin` ou `Julie-Martin` oui.

Si vous créez un compte, des identifiants vous seront proposés. Utilisez-les (copier-coller) tout de suite après, lorsque Silkaj vous les demandera. Rien ne sera affiché. Conservez-les précieusement, par exemple dans un gestionnaire de mots de passe.

### Sur un ordinateur personnel
Ğ1Cotis est plutôt conçu pour fonctionner sur serveur. Cependant, vous pouvez utiliser la fonctionnalité "cotisation" en l'installant sur un PC simple, **en choisissant la fréquence *reboot***


## Fichiers

Les dossiers seront créés à la première utilisation de Ğ1cotis.
L'organisation des dossiers est prévue ainsi :

* User 
    - G1cotis   
        - README
        - LICENCE
        - config.sh
        - nouveau-compte.sh
        
        - auth-g1cotis (fichiers d'authentification)
            - compte1.auth  
            - compte2.auth  
            - ...
        - d_(periode) (fichiers de configuration des comptes)
            - cotisations
                - compte1.sh, compte2.sh, ...
            - recurrent
                - compte3.sh ; compte4.sh, ...
        - logs
            - g1cotis.logs (rotation bimensuelle des logs)
## Autres

NB : le service de virement récurrent ne gère pas d'alerte courriel si le compte est vide. 
D'autre part, il ne peut pas être lancé comme script fonctionnant au reboot, il tourne forcément sur un serveur.

Je ne proposerai pas de service ouvert, cependant j'invite qui le souhaite à proposer ce service à ses connaissances. Si vous le faites, j'aurais plaisir à le savoir ;-)
      
### Licence CECILL ###

Cet ensemble de scripts est publié sous licence CECILL 2.1, compatible avec la licence GNU.
Aucune garantie n'est apportée.
