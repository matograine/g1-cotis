#!/bin/bash
## Script de lancement des scripts G1Cotis

#fichier de conf
config=../config.sh

# sources
source $config
source $dossier_scripts/fonctions.sh

#debug
echo "=================="
echo "MENSUEL"
echo "date : " $(date)
echo "PATH:" $PATH
echo "source" $config
echo "noeud" $noeud
echo "=================="

# Configuration

if [ ! "$(echo $dossier_mens/cotisations/*)" == "$dossier_mens/cotisations/*" ] ; then
        for i in $dossier_mens/cotisations/*.sh ; do
                source $i
                source $g1cotis
        done
fi

if [ ! "$(echo $dossier_mens/recurrent/*)" == "$dossier_mens/recurrent/*" ] ; then
        for i in $dossier_mens/recurrent/*.sh ; do
                source $i
                source $g1recur
        done
fi


# Rotation des logs
rm $dossier/logs/g1cotis.log.OLD
mv $dossier/logs/g1cotis.log $dossier/logs/g1cotis.log.OLD