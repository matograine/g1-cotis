#!/bin/bash
## Script de lancement des scripts G1Cotis

#fichier de conf
config=../config.sh

# sources
source $config
source $dossier_scripts/fonctions.sh

#debug
echo "=================="
echo "REBOOT"
echo "date : " $(date)
echo "PATH:" $PATH
echo "source" $config
source $config
echo "noeud" $noeud
echo "=================="

# Configuration

if [ ! "$(echo $dossier_reboot/cotisations/*)" == "$dossier_reboot/cotisations/*" ] ; then
        for i in $dossier_reboot/cotisations/*.sh ; do
                source $i
                source $g1cotis
        done
fi


