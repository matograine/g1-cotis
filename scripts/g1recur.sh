#!/bin/bash

## Ce script sert à gérer des versements récurrents en Ğ1.
## Il va transférer la monnaie d'un compte "réserve" vers un compte destinataire, par montants fixes.

## Il se lance depuis init_g1cotis.sh.

## Ce script prévoit la gestion d'un commentaire dans le fichier du compte, de forme com="commentaire" SANS ACCENT, APOSTROPHE, etc...
## La configuration de ce comentaire fait directement dans le fichier et n'est pas prévue dans le script de configuration.

## Chemin du dossier où est stocké le fichier d'identification du compte intermédiaire
auth=$dossier_auth/$recept.auth		# fichier d'authentification du compte intermédiaire, de forme <CLEF_PUB>.auth

echo "===================="
echo "compte $compte : $recept"

####### Début du script #######

## Vérification de la cohérence des données
## Pas de variable vide
for i in $recept $virt_recur ; do
	if [ $i == "" ] ; then
		echo "Les informations sont incomplètes, veuillez modifier ou recréer le fichier. (compte : $compte ; clef pub : $recept)" ; continue
	fi
done

## existence fichier d'auth
if [ ! -e "$dossier_auth/$recept.auth" ] ; then
	echo "Il n'existe pas de fichier d'authentification pour la clef publique $recept. Veuillez en créer un ou modifier le compte $compte." ; continue
fi

## Montant
while true ; do
        montant=$(silkaj $ep $noeud balance $recept | grep "Total amount (unit|relative)" | sed -e "s/│ Total amount (unit|relative)\s*│//g" | sed -e "s/$monnaie.*$//" | bc)
	if [ "$?" == "0" ] ; then
		break
	else
		echo "ERREUR : nouvel essai dans 5 minutes. Si ce problème se reproduit, envisagez de changer de noeud en modifiant config.sh"
                sleep 300
	fi
done

montant_round=$(echo "$montant" | awk '{printf("%d\n",$1)}')

## On lance les cotis s'il y a suffisamment de monnaie
if [ $montant_round -eq 0 ] ; then            # S'il n'y a pas de monnaie
    echo "montant nul"
	# gérer l'envoi de courriels
elif [ $(echo "100 * $montant_round" | bc) -lt $(echo "100 * $virt_recur" | bc) ] ; then         # S'il n'y a pas assez de monnaie
    echo "montant insuffisant"
	# gérer l'envoi de courriels
elif [ $(echo "100 * $montant_round" | bc) -ge $(echo "100 * $virt_recur" | bc) ] ; then
	echo "montant : " $montant $monnaie

## Envoi du virement : 5 tentatives
        a=0
        while [ $a -le 5 ] ; do
            silkaj $ep $noeud -af --file $auth tx -y -r "$fin" -a "$virt_recur"  -c "$com"
            if [ $? == 0 ] ; then
                echo "virement effectué" ; break
            elif [ $a -eq 5 ] ; then
                echo "Le serveur est inaccessible. envisagez de changer de noeud en modifiant config.sh ou reportez ce bug."
            else
                echo "ERREUR : nouvel essai dans 5 minutes. Si ce problème se reproduit, envisagez de changer de noeud en modifiant config.sh ; ou vérifiez que le versement a DEUX chiffres après la virgule."
                a=$(($a+1))
                sleep 300
            fi
        done
fi

# vidage des variables
recept=""
compte=""
fin=""
virt_recur=""
