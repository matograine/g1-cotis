# Regrouper les fonctions.
# On appelle ce script avec `source $dossier_scripts/fonctions.sh`

ecriture_liste ()
{
lst=( $@ )
n=0
while [ $n -lt ${#lst[@]} ] ; do
        echo ${lst[$n]} >> $fichier
        n=$((n+1))
done
}

#tx_verif ()

adr_verif ()
{
echo "Vérification de l'adresse..."
silkaj $ep $noeud balance "$1" | grep Total > /dev/null
return $?
}


tps_random ()   # paramètres :  maximum
{
tps=$RANDOM
tps=$(( $tps % $1 ))
return $tps
}
