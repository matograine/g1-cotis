#!/bin/bash

## Ce script sert à gérer des cotisations volontaires en Ğ1.
## Il surveille le montant d'un compte de réception des paiements,
## calcule le montant d'une "taxe" à envoyer à un ou des comptes choisis
## envoie cette taxe
## envoie le reste au compte destinataire définitif.

## Il se lance depuis init_g1cotis.sh.
## Ce script prévoit la gestion d'une liste de commentaires dans le fichier du compte, de forme "lst_com=( com_1 com_2 ... )" SANS ACCENT, APOSTROPHE, etc...
## La configuration de cette liste se fait directement dans le fichier.

## Chemin du dossier où est stocké le fichier d'identification du compte intermédiaire
auth=$dossier_auth/$recept.auth		# fichier d'authentification du compte intermédiaire, de forme <CLEF_PUB>.auth


#### Log  ####
echo "===================="
echo "compte $compte : $recept"

####### Début du script #######

## Vérification de la cohérence des données
## Pas de variable vide
for i in $recept $min $fin $lst_tx $lst_p ; do
	if [ $i == "" ] ; then
		echo "Les informations sont incomplètes, veuillez modifier ou recréer le fichier. (compte : $compte ; clef pub : $recept)" ; continue
	fi
done

## existence fichier d'auth
if [ ! -e "$dossier_auth/$recept.auth" ] ; then
	echo "Il n'existe pas de fichier d'authentification pour la clef publique $recept. Veuillez en créer un ou modifier le compte $compte." ; continue
fi

## Cohérence des listes
if [ ! ${#lst_tx[@]} == ${#lst_p[@]} ] ; then
	echo "Vos listes (cotisations, pourcentages) contiennent un nombre différent d'éléments. Veuillez vérifier le compte $compte." ; continue
fi

if [ ! "$lst_com" == "" ] && [ ! ${#lst_tx[@]} == ${#lst_com[@]} ] ; then
	echo "Le nombre de commentaires est incohérent avec le nombre de cotisations. Veuillez vérifier le compte $compte." ; continue
fi

## Réserve
## Montant
while true ; do
	montant=$(silkaj $ep $noeud balance $recept | grep "Total amount (unit|relative)" | sed -e "s/│ Total amount (unit|relative)\s*│//g" | sed -e "s/$monnaie.*$//" | bc)
	if [ "$?" == "0" ] ; then
		break
	else
		echo "ERREUR : nouvel essai dans 5 minutes. Si ce problème se reproduit, envisagez de changer de noeud en modifiant config.sh"
                sleep 300
	fi
done

montant_round=$(echo "$montant" | awk '{printf("%d\n",$1)}')

## On lance les cotis s'il y a plus de 100 Ğ1
if [ $montant_round -eq 0 ] ; then            # S'il n'y a pas de monnaie
        echo "montant nul"
elif [ $montant_round -lt $min ] ; then         # S'il n'y a pas assez de monnaie
        echo "montant insuffisant"
elif [ $montant_round -ge $min ] ; then
	echo "montant : " $montant $monnaie

## Création de la liste de récipiendaires
    recipients="-r ${lst_tx[0]}"
    a=1
    while [ $a -lt ${#lst_tx[@]} ] ; do
        recipients="$recipients -r ${lst_tx[$a]}"
        a=$((a+1))
    done
    recipients="$recipients -r $fin"

## Création de la liste de montants

    first_amount=$(echo "$montant * ${lst_p[0]} * 0.01 " | bc )
    amount="-a $first_amount"
    restant=$(echo "$montant - $first_amount" | bc)
    a=1
    while [ $a -lt ${#lst_p[@]} ] ; do
        p=${lst_p[$a]}
        cot=$(echo "$montant * $p * 0.01 " | bc )
        amount="$amount -a $cot"
        restant=$(echo "$restant - $cot" | bc )
        a=$((a+1))
    done
    amount="$amount -a $restant"

## Commentaire
    if [[ $comment ==  "" ]] ; then
        comment="G1Cotis"
    fi

## envoi transaction
    silkaj $ep $noeud -af --file $auth tx -y $recipients $amount -c $comment

## fin du script
fi

# vidage des variables
recept=""
compte=""
fin=""
min=""
lst_tx=""
lst_p=""
comment=""
amount=""
recipients=""

