#!/bin/sh
nomduscript=nouveau-compte_cotis.sh
## Ce script vous permet de configurer rapidement votre instance G1Cotis.
## Il est supposé être appelé par nouveau-compte.sh.

## Choix de la périodicité
echo  "Vous voulez créer un nouveau compte de COTISATION."
printf "\nQuelle périodicité voulez-vous lui donner ?\n 1 - Quotidien\n 2 - Hebdomadaire\n 3 - Mensuel\n 4 - Au reboot\n (1/2/3/4) : "
read choix
case $choix in
1)
dossier_cot=$dossier_quot/cotisations
;;
2)
dossier_cot=$dossier_hebdo/cotisations
;;
3)
dossier_cot=$dossier_mens/cotisations
;;
4)
dossier_cot=$dossier_reboot/cotisations
;;
esac

echo -n "Quel nom voulez-vous lui donner ? (Le nom ne doit pas contenir d'espace): "
read nom ; fichier=$dossier_cot/$nom.sh
if [ -e $fichier ] ; then
	printf "\nLe fichier existe déjà. Une nouvelle configuration effacera la configuration actuelle.\nÊtes-vous sûr.e de vouloir continuer (o/N) ? " ; read ouinon
		if [ "$ouinon" == "o" ] || [ "$ouinon" == "O" ] ; then
			rm $fichier ; touch $fichier ; echo "Remplacement de $nom.sh"
		else
			printf "\nD'accord, au revoir !\n" ; exit
		fi
else
	touch $fichier
	printf "Création de $nom.sh"
fi

## intro du fichier ##

printf "#!/bin/bash \n
## Configuration du compte Ğ1Cotis $nom
## Le compte intermédiaire reçoit les paiements, puis envoie les cotisations aux comptes choisis et le restant au compte final.
## On utilise pour ce compte un fichier d'authentification, stocké dans un dossier spécifique.
## Le fichier d'authentification doit être de forme <CLEF_PUBLIQUE>.auth
## Vous pouvez le générer sur Silkaj par la commande 'silkaj authfile'. Prenez soin de le renommer et le déplacer dans le bon dossier par la suite.\n\n
## Adresse publique du compte intermédiaire\n" >> $fichier

## fin de l'intro ##


# Avez-vous créé un compte intermédiaire ? Si oui -> génération du fichier d'authentification avec authentification manuelle // Si non -> suggestion d'un couple id/mdp et création fichier d'authentification.
printf 	"\n\n###############################################\n#### Configuration du compte intermédiaire ####\n############################################### \n\n
Vous allez configurer un compte intermédiaire pour $nom. "

while true ; do
echo -n "Connaissez-vous déjà les identifiants pour ce compte ? (o/N) : "
read ouinon
	if [ "$ouinon" == "o" ] || [ "$ouinon" == "O" ] ; then
		printf "\nNous allons créer le fichier d'authentification pour ce compte. Soyez sûr.e de vous rappeler des identifiants, Ğ1Cotis ne les enregistrera pas.\nEntrez les identifiants lorsque Silkaj vous le demandera.\n\nEn attente de Silkaj...\n\n"
		rm -f ./authfile ; silkaj authfile > /tmp/$nom
		pubkey=$(cat /tmp/$nom | grep "Authentication file 'authfile' generated and stored in current folder for following public key:" | cut -d':' -f2 | sed 's/.\{1\}//'  )	#| cut -d' ' -f2- | sed -e 's/ $monnaie//g'  )
		printf "\nClef publique : $pubkey pour le compte $nom. Est-ce juste ? (o/N) : "
		read ouinon
		if [ "$ouinon" = "o" ] || [ "$ouinon" = "O" ]; then
			printf "recept=$pubkey\n" >> $fichier
			mv ./authfile $dossier_auth/$pubkey.auth
			printf "Parfait, continuons\n\n" ; break
		elif [ "$ouinon" = "n" ] || [ "$ouinon" = "N" ] || [ "$ouinon" = "" ]; then
			printf "\nRecommençons."
		fi

	elif [ "$ouinon" == "n" ] || [ "$ouinon" == "N" ] || [ "$ouinon" == "" ] ; then
		echo "Création d'identifiants aléatoires..."
		id=$(diceware -d - --no-caps)
		mdp=$(diceware -d - --no-caps)
		printf "\nNous vous proposons un identifiant secret et un mot de passe, basés sur Diceware. \nEcrivez-les (copier-coller) quand Silkaj vous les demandera.\nRappel : <Ctrl+Maj+C> et <Ctrl+Maj+V> pour copier-coller dans le terminal.\nSoyez sûr.e de vous en rappeler, Ğ1Cotis ne les enregistrera pas. \n\nID (à copier-coller)= $id\nMDP (à copier-coller)= $mdp\n\nEn attente de Silkaj...\n\n"
		rappel=yes
		rm -f ./authfile ; silkaj authfile > /tmp/$nom
		pubkey=$(cat /tmp/$nom | grep "Authentication file 'authfile' generated and stored in current folder for following public key:" | cut -d':' -f2 | sed 's/.\{1\}//' )	#| cut -d' ' -f2- | sed -e 's/ $monnaie//g'  )
		printf "recept=$pubkey\n" >> $fichier
		mv ./authfile $dossier_auth/$pubkey.auth
		printf "\nClef publique : $pubkey pour le compte $nom.\n\n" ; break
	else
		printf "Il faut écrire O ou N, pas $ouinon !"
	fi
done
printf "compte=$nom\n\n" >> $fichier
rm /tmp/$nom

echo -n "Par défaut, Ğ1Cotis se déclenche si le compte intermédiaire contient plus de 100 $monnaie. Voulez-vous changer ce montant minimal ? (o/N) : "
read ouinon
if [ "$ouinon" == "o" ] || [ "$ouinon" == "O" ] ; then
	echo -n "Quel montant minimal doit contenir le compte, en $monnaie ? : "
	read min
	printf "# Montant minimal pour le déclenchement de Ğ1Cotis\nmin=$min\n\n" >> $fichier
	printf "Montant minimal de $min $monnaie.\n"
else
	printf "# Montant minimal pour le déclenchement de Ğ1Cotis\nmin=100\n\n" >> $fichier
	printf "Montant minimal de 100 $monnaie\n"
fi

printf 	"\n#####################################################\n#### Configuration du compte de réception finale ####\n##################################################### \n\n"
while true ; do
	printf "Quelle est l'adresse de réception finale ? : "
	read fin
	adr_verif "$fin"
	if [ $? == 0 ] ; then
		printf "L'adresse $fin est valide, continuons.\n" ; break
	else
		printf "l'adresse $fin est invalide. Veuillez réessayer, ou configurer un noeud Duniter connecté. (Ctrl+C pour sortir)\n\n"
	fi
done

printf "## Adresse du compte qui recevra le versement final\nfin=$fin\n\n" >> $fichier

printf 	"\n##################################################\n#### Configuration des comptes de cotisation ####\n################################################## \n\n"
printf "Nous allons configurer les comptes de cotisation."
printf "#### Définition des comptes de cotisation ####\n\n## La définition des comptes de cotisation doit être de forme : tx_n=CLEF_PUB ; p_n=POURCENTAGE\n## On doit lister ces comptes et en indiquer le nombre.\n## Je maîtrise mal les listes en Bash, une aide sur ce point du code serait grandement appréciée\n\n## Adresses publiques et pourcentage (en %%)\n\n" >> fichier.txt

nbr=1
lst_tx=()
lst_p=()

while true ; do
	printf "\n\nCotisation n° $nbr.\n===============\nEntrez une adresse de cotisation, ou <ENTREE> pour terminer : "
	read cot
	if [ "$cot" == "" ] ; then
		printf "Fin de configuration des comptes de cotisation.\n"
		break
	else
		adr_verif "$cot"
		if [ $? == 0 ] ; then
			printf "$cot est une adresse valide.\n"
			printf "Quel pourcentage voulez-vous lui attribuer ? : "
			read pourcent
			lst_tx[$(($nbr))]=$cot
			lst_p[$(($nbr))]=$pourcent
			printf "Verser $pourcent%% à l'adresse $cot.\n\n"
			nbr=$(($nbr+1))
		else
			echo "$cot n'est pas une adresse valide, veuillez vérifier et donner une adresse valide, ou configurer un noeud Duniter disponible dans config.sh"
		fi
	fi
done

## Vérification des pourcentages
for i in ${lst_p[@]} ; do
	tot=$(($tot + $i))
	if [ $tot -gt 100 ] ; then
		rm $fichier
		printf "\n\nERREUR : Le total dépasse 100%%. **Veuillez relancer le script nouveau-compte.sh et vérifier vos pourcentages**.\n\n" ; exit
		continue
	fi
done

printf "## Liste des comptes de cotisation\nlst_tx=(\n" >> $fichier
ecriture_liste ${lst_tx[@]}
printf ")\n\n" >> $fichier

printf "## Liste des pourcentages\nlst_p=(\n" >> $fichier
ecriture_liste ${lst_p[@]}
printf ")\n\n" >> $fichier

printf "## Commentaire\ncomment=G1Cotis" >> $fichier
