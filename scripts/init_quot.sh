#!/bin/bash
## Script de lancement des scripts G1Cotis

#fichier de conf
config=../config.sh

# sources
source $config
source $dossier_scripts/fonctions.sh

#debug
echo "=================="
echo "QUOTIDIEN"
echo "date : " $(date)
echo "PATH:" $PATH
echo "source" $config
echo "noeud" $noeud
echo "=================="

# Configuration

if [ ! "$(echo $dossier_quot/cotisations/*)" == "$dossier_quot/cotisations/*" ] ; then
        for i in $dossier_quot/cotisations/*.sh ; do
                source $i
                source $g1cotis
        done
fi

if [ ! "$(echo $dossier_quot/recurrent/*)" == "$dossier_quot/recurrent/*" ] ; then
        for i in $dossier_quot/recurrent/*.sh ; do
                source $i
                source $g1recur
        done
fi

