#!/bin/bash
## Script de lancement des scripts G1Cotis

#fichier de conf
config=../config.sh

# sources
source $config
source $dossier_scripts/fonctions.sh

#debug
echo "=================="
echo "HEBDOMADAIRE"
echo "date : " $(date)
echo "PATH:" $PATH
echo "source" $config
echo "noeud" $noeud
echo "=================="



# Configuration

if [ ! "$(echo $dossier_hebdo/cotisations/*)" == "$dossier_hebdo/cotisations/*" ] ; then
        for i in $dossier_hebdo/cotisations/*.sh ; do
                source $i
                source $g1cotis
        done
fi

if [ ! "$(echo $dossier_hebdo/recurrent/*)" == "$dossier_hebdo/recurrent/*" ] ; then
        for i in $dossier_hebdo/recurrent/*.sh ; do
                source $i
                source $g1recur
        done
fi


