#!/bin/sh
nomduscript=nouveau-compte_recur.sh
## Ce script vous permet de configurer rapidement votre instance G1Cotis.
## Il est supposé être appelé par nouveau-compte.sh

## Choix de la périodicité
echo -n  "Vous voulez créer un nouveau compte de VIREMENT RECURRENT. Cette fonction n'est possible que si Ğ1Cotis est installé sur un serveur allumé en permanence."
printf "\nQuelle périodicité voulez-vous lui donner ?\n 1 - Quotidien\n 2 - Hebdomadaire\n 3 - Mensuel\n (1/2/3) : "
read choix
case $choix in
1)
dossier_cot=$dossier_quot/recurrent
;;
2)
dossier_cot=$dossier_hebdo/recurrent
;;
3)
dossier_cot=$dossier_mens/recurrent
;;
esac

printf "\n\nQuel nom voulez-vous lui donner ? (Le nom ne doit pas contenir d'espace) : "
read nom ; fichier=$dossier_cot/$nom.sh
if [ -e $fichier ] ; then
	printf "\nLe fichier existe déjà. Une nouvelle configuration effacera la configuration actuelle.\nÊtes-vous sûr.e de vouloir continuer (o/N) ? "
	read ouinon
		if [ "$ouinon" == "o" ] || [ "$ouinon" == "O" ] ; then
			rm $fichier ; touch $fichier ; echo "Remplacement de $nom.sh"
		else
			printf "\nD'accord, au revoir !\n" ; exit
		fi
else
	touch $fichier
	printf "\nCréation de $nom.sh"
fi

## intro du fichier ##

printf "#!/bin/bash \n
## Configuration du compte Ğ1Cotis $nom
## Le compte intermédiaire reçoit les paiements, puis envoie un montant fixe vers un compte destinataire.
## On utilise pour ce compte un fichier d'authentification, stocké dans un dossier spécifique.
## Le fichier d'authentification doit être de forme <CLEF_PUBLIQUE>.auth
## Vous le générez sur Silkaj par la commande 'silkaj generate_auth_file'. Prenez soin de le renommer et le déplacer dans le bon dossier par la suite.\n\n
## Adresse publique du compte intermédiaire\n" >> $fichier

## fin de l'intro ##


# Avez-vous créé un compte intermédiaire ? Si oui -> génération du fichier d'authentification avec authentification manuelle // Si non -> suggestion d'un couple id/mdp et création fichier d'authentification.
printf 	"\n\n###############################################\n#### Configuration du compte intermédiaire ####\n############################################### \n\n
Vous allez configurer un compte intermédiaire pour $nom. "

while true ; do
echo -n "Connaissez-vous déjà les identifiants pour ce compte ? (o/N) : "
read ouinon
	if [ "$ouinon" == "o" ] || [ "$ouinon" == "O" ] ; then
		printf "\nNous allons créer le fichier d'authentification pour ce compte. Soyez sûr.e de vous rappeler des identifiants, Ğ1Cotis ne les enregistrera pas.\nEntrez les identifiants lorsque Silkaj vous le demandera.\n\nEn attente de Silkaj ...\n\n"
		rm -f ./authfile ; silkaj authfile > /tmp/$nom
		pubkey=$(cat /tmp/$nom | grep "Authentication file 'authfile' generated and stored in current folder for following public key:" | cut -d':' -f2 | sed 's/.\{1\}//'  )	#| cut -d' ' -f2- | sed -e 's/ $monnaie//g'  )
		printf "\nClef publique : $pubkey pour le compte $nom. Est-ce juste ? (o/N) : "
		read ouinon
		if [ "$ouinon" = "o" ] || [ "$ouinon" = "O" ]; then
			printf "recept=$pubkey\n" >> $fichier
			mv ./authfile $dossier_auth/$pubkey.auth
			printf "Parfait, continuons\n\n" ; break
		elif [ "$ouinon" = "n" ] || [ "$ouinon" = "N" ] || [ "$ouinon" = "" ]; then
			printf "\nRecommençons."
		fi

	elif [ "$ouinon" == "n" ] || [ "$ouinon" == "N" ] || [ "$ouinon" == "" ] ; then
		echo "Création d'identifiants aléatoires..."
		id=$(diceware -d - --no-caps)
		mdp=$(diceware -d - --no-caps)
		printf "\nNous vous proposons un identifiant secret et un mot de passe, basés sur Diceware. \nEcrivez-les (copier-coller) quand Silkaj vous les demandera.\nRappel : <Ctrl+Maj+C> et <Ctrl+Maj+V> pour copier-coller dans le terminal.\nSoyez sûr.e de vous en rappeler, Ğ1Cotis ne les enregistrera pas. \n\nID (à copier-coller)= $id\nMDP (à copier-coller)= $mdp\n\nEn attente de Silkaj ... \n\n"
		rappel=yes
		rm -f ./authfile ; silkaj authfile > /tmp/$nom
		pubkey=$(cat /tmp/$nom | grep "Authentication file 'authfile' generated and stored in current folder for following public key:" | cut -d':' -f2 | sed 's/.\{1\}//' )	#| cut -d' ' -f2- | sed -e 's/ $monnaie//g'  )
		printf "recept=$pubkey\n" >> $fichier
		mv ./authfile $dossier_auth/$pubkey.auth
		printf "\nClef publique : $pubkey pour le compte $nom.\n\n" ; break
	else
		printf "Il faut écrire O ou N, pas $ouinon !"
	fi
done
printf "compte=$nom\n\n" >> $fichier
rm /tmp/$nom

printf 	"\n##############################################\n#### Configuration du compte destinataire ####\n############################################## \n\n"
while true ; do
	printf "Quelle est l'adresse vers laquelle vous voulez envoyer les virements récurrents ? : "
	read fin
	adr_verif "$fin"
	if [ $? == 0 ] ; then
		printf "L'adresse $fin est valide, continuons.\n" ; break
	else
		printf "l'adresse $fin est invalide. Veuillez réessayer, ou configurer un noeud Duniter connecté. (Ctrl+C pour sortir)\n\n"
	fi
done

printf "## Adresse du compte destinataire :\nfin=$fin\n\n" >> $fichier

printf 	"\n##################################\n#### Configuration du montant ####\n################################# \n\n"

while true ; do
	printf "Quel montant voulez-vous verser ? : "
	read virt_recur
	virt_round=$(echo "$virt_recur" | awk '{printf("%d\n",$1)}')
	if [ $virt_round -le 0 ] ; then
		echo "le montant est nul ou négatif ! Veuillez recommencer."
	else
		break
	fi
done
printf "## Montant du virement ##\nvirt_recur=$virt_recur\n\n" >> $fichier
echo "Montant $virt_recur enregistré."
