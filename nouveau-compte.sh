#!/bin/sh
nomduscript=nouveau-compte.sh
## Ce script vous permet de configurer rapidement votre instance G1Cotis.
## Vous pouvez également éditer manuellement le fichier config.sh,je n'ai sans doute pas prévu tous les cas d'usage !

#### Paramètres par défaut
doss=$(pwd)
source ./config.sh
source $dossier_scripts/fonctions.sh

#### Début du script ####

# 1- information

printf  "\n##################################\n#### Configuration de Ğ1Cotis ####\n################################## \n\n"
printf  "Ğ1Cotis est un système de cotisations volontaires en monnaie libre.

Ğ1Cotis est un script bash, conçu pour être installé sur des systèmes de type GNU/Linux. 
Vous pouvez utiliser Ğ1Cotis pour votre usage personnel, ou proposer à d'autres personnes de gérer leurs cotisations par ce système.
En effet, un même ordinateur peut servir pour plusieurs comptes.

Ğ1Cotis dépend du logiciel Silkaj : installez-le avant de faire autre chose. site web : silkaj.duniter.org
Ğ1Cotis dépend également du paquet Diceware, communément disponible.

Ğ1Cotis  s'exécute par une commande CRON. Si vous ne savez pas ce que c'est, nous vous guiderons à la fin du processus.\n

Avez-vous lu le README, installé les dépendances et configuré config.sh ? (O/n) : "
read ouinon
if [ "$ouinon" == "n" ] || [ "$ouinon" == "N" ]  ; then
        echo "D'accord. Faites-le, puis redémarrez nouveau-compte.sh. A bientôt !"
fi

# 2- initialisation : je récupère les infos que je peux avoir automatiquement.

printf "\n########################################\n####    Initialisation du script    ####\n########################################\n\n"
# 3- questions :
        # lecture du chemin du dossier, Vérification du dossier
if [ !  $doss == $dossier ] ; then
        printf "Vous n'avez pas modifié le fichier config.sh. \n \n Veuillez y indiquer : \'dossier=$doss\' , et vérifier le reste de la configuration. \n Vérifiez également que vous avez installé Silkaj et Diceware, comme indiqué dans le Readme. Si besoin, modifiez config.sh en fonction de votre installation de Silkaj. \n Puis relancer nouveau-compte.sh. A tout de suite !\n"
        exit
fi


# VERIFIER L'EXISTENCE DES DOSSIERS

if [ ! -e $dossier_quot ] || [ ! -e $dossier_hebdo ] || [ ! -e $dossier_mens ] || [ ! -e $dossier_reboot ] || [ ! -e $dossier_auth ] || [ ! -e $dossier/logs ] ; then
        mkdir $dossier_quot $dossier_quot/cotisations $dossier_quot/recurrent
        mkdir $dossier_hebdo $dossier_hebdo/cotisations $dossier_hebdo/recurrent
        mkdir $dossier_mens $dossier_mens/cotisations $dossier_mens/recurrent
        mkdir $dossier_reboot $dossier_reboot/cotisations
        mkdir $dossier/logs
        mkdir $dossier_auth
fi

## Choix du type de virement
echo -n "Voulez-vous créer un compte de cotisations (1) ou un virement récurrent (2) ? (1/2) : "
read type
if [ "$type" == "1" ] ; then
        source $dossier_scripts/nouveau-compte_cotis.sh
elif [ "$type" == "2" ] ; then
        source $dossier_scripts/nouveau-compte_recur.sh
else
        echo "Tapez 1 ou 2 ! Veuillez recommencer." ; exit
fi

## confirmation
printf "\n\n#######################\n#### Vérifications ####\n#######################\n\n"

## identifiants
if [ ! $id == "" ] ; then
        printf "Soyez certain.e de vous souvenir de vos identifiants. Voulez-vous les afficher de nouveau ? (o/n) : "
        read ouinon
        if [ "$ouinon" == "n" ] || [ "$ouinon" == "N" ] ; then
                printf "\n\n"
        else
                printf "\nID= $id\nMDP= $mdp\n\n"
        fi
fi

## verifier le fichier
printf "Vous allez vérifier les paramètres du compte.\nSi ces paramètres ne correspondent pas, vous pouvez relancer le script nouveau-compte.sh ou modifier directement $fichier.\n\n"
printf "OK, continuer : "
        read continuer
printf "\n\n#### Début du fichier ####\n"
printf '\E[31;40m'"\033[1m$(cat $fichier)\033[0m"
printf "\n\n#### fin du fichier ####\n\n"
printf "#### Dernières vérifications... ####\n\n"

## Vérification crontab
crontab -l | grep g1cotis > /tmp/g1cotis-crontab
if [ ! "$?" == "0" ] ; then
        printf "Voulez-vous lancer Ğ1Cotis automatiquement ? (O/n) : "
        read ouinon
        if [ "$ouinon" == "n" ] || [ "$ouinon" == "N" ] ; then
                printf "\nD'accord, au revoir !"
        else
                cr_reboot=$(echo "@reboot sleep 600 ; cd $dossier_scripts ; bash ./init_reboot.sh >> $dossier/logs/g1cotis.log  2>&1")

                tps_random 60 ; m=$?
                tps_random 24 ; h=$?
                dom=* ; dow=*
                cr_quot=$(echo "$m $h $dom * $dow cd $dossier_scripts ; bash ./init_quot.sh >> $dossier/logs/g1cotis.log 2>&1")
                printf "\nĞ1Cotis (quotidien) sera lancé chaque jour à $h:$m."

                tps_random 60 ; m=$?
                tps_random 24 ; h=$?
                tps_random 7 ; dow=$?
                dom=*
                cr_hebdo=$(echo "$m $h $dom * $dow cd $dossier_scripts ; bash ./init_hebdo.sh >> $dossier/logs/g1cotis.log 2>&1")
                printf "\nĞ1Cotis (hebdomadaire)  sera lancé chaque $dow e jour de la semaine à $h:$m. ( 0 ou 7 : dimanche)"

                tps_random 60 ; m=$?
                tps_random 24 ; h=$?
                tps_random 28 ; dom=$?
                dow=*
                cr_mens=$(echo "$m $h $dom * $dow cd $dossier_scripts ; bash ./init_mens.sh >> $dossier/logs/g1cotis.log 2>&1")
                printf "\nĞ1Cotis (mensuel) sera lancé chaque $dow e jour du mois à $h:$m."
        printf '\E[31;40m'"\033[1m\n\n !! ATTENTION !! : Après ce script, exécutez la commande 'crontab -e' et ajoutez à la fin de votre crontable (modifiez MAILTO si vous savez ce que vous faites) :\n\nMAILTO=\"\"\n$cr_reboot\n$cr_quot\n$cr_hebdo\n$cr_mens\n\n\033[0m "
        fi
else
        printf "Votre Crontab est déjà configuré. \nSi vous installez la v0.2 par-dessus la v0.1, veuillez effacer les lignes correspondantes dans le crontab et recommencer."
fi
rm /tmp/g1cotis-crontab

printf "\n\n#####\nFin des vérifications. Suivez nos recommandations avant le premier lancement de Ğ1Cotis.\n\nMerci de participer au financement de la Ğ1 !\n\n"
